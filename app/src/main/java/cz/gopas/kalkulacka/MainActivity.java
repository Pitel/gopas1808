package cz.gopas.kalkulacka;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity implements CalcFragment.AboutListener {
	private static final String TAG = MainActivity.class.getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		FragmentManager.enableDebugLogging(BuildConfig.DEBUG);
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.replace(android.R.id.content, new CalcFragment())
					.commit();
		}
		Toast.makeText(this, "onCreate", Toast.LENGTH_SHORT).show();
		Log.d(TAG, "onCreate");

		Log.d(TAG, String.valueOf(getIntent().getDataString()));
	}

	@Override
	protected void onStart() {
		super.onStart();
		Toast.makeText(this, "onStart", Toast.LENGTH_SHORT).show();
		Log.d(TAG, "onStart");
	}

	@Override
	protected void onResume() {
		super.onResume();
		Toast.makeText(this, "onResume", Toast.LENGTH_SHORT).show();
		Log.d(TAG, "onResume");
	}

	@Override
	protected void onPause() {
		Toast.makeText(this, "onPause", Toast.LENGTH_SHORT).show();
		Log.d(TAG, "onPause");
		super.onPause();
	}

	@Override
	protected void onStop() {
		Toast.makeText(this, "onStop", Toast.LENGTH_SHORT).show();
		Log.d(TAG, "onStop");
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		Toast.makeText(this, "onDestroy", Toast.LENGTH_SHORT).show();
		Log.d(TAG, "onDestroy");
		super.onDestroy();
	}

	@Override
	public void showAbout() {
		Log.d(TAG, "About");
		getSupportFragmentManager().beginTransaction()
				.replace(android.R.id.content, new AboutFragment())
				.addToBackStack(null)
				.commit();
	}
}
