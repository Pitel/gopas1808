package cz.gopas.kalkulacka;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface ConstantDao {
	@Insert(onConflict = OnConflictStrategy.REPLACE)
	void insert(ConstantModel... constants);

	@Query("SELECT * FROM constantmodel")
	List<ConstantModel> getAll();
}
