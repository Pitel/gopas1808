package cz.gopas.kalkulacka;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AboutListener} interface
 * to handle interaction events.
 */
public class CalcFragment extends Fragment {
	private static final String TAG = CalcFragment.class.getSimpleName();
	private AboutListener mListener;

	@BindView(R.id.a_txt)
	EditText a;
	@BindView(R.id.b_txt)
	EditText b;
	@BindView(R.id.ops)
	RadioGroup ops;
	@BindView(R.id.result)
	TextView result;

	private SharedPreferences prefs;

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.activity_main, container, false);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		ButterKnife.bind(this, view);
		setHasOptionsMenu(true);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.about:
				Log.d(TAG, "About");
				mListener.showAbout();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof AboutListener) {
			mListener = (AboutListener) context;
		} else {
			throw new RuntimeException(context.toString()
					+ " must implement AboutListener");
		}
		prefs = PreferenceManager.getDefaultSharedPreferences(context);
	}

	@OnClick(R.id.fab)
	void calc() {
		Log.d(TAG, "Click!");
		float x = Float.NaN;
		float a = Float.parseFloat(this.a.getText().toString());
		float b = Float.parseFloat(this.b.getText().toString());
		switch (ops.getCheckedRadioButtonId()) {
			case R.id.add:
				x = a + b;
				break;
			case R.id.sub:
				x = a - b;
				break;
			case R.id.mul:
				x = a * b;
				break;
			case R.id.div:
				if (b == 0) {
					new ZeroDialog().show(getFragmentManager(), null);
				}
				x = a / b;
				break;
		}
		result.setText(String.valueOf(x));
		prefs.edit()
				.putString("mem", result.getText().toString())
				.apply();
	}

	@OnClick(R.id.share)
	void share() {
		Intent intent = new Intent(Intent.ACTION_SEND)
				.setType("text/plain")
				.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_txt, result.getText()));
		startActivity(intent);
	}

	@OnClick(R.id.mem)
	void mem() {
		b.setText(prefs.getString("mem", "0"));
	}

	@OnClick(R.id.constant)
	void showConstant() {
		startActivityForResult(new Intent(getContext(), ConstantActivity.class), 25);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d(TAG, requestCode + " " + resultCode);
		switch (requestCode) {
			case 25:
				if (resultCode == Activity.RESULT_OK) {
					float result = data.getFloatExtra("result", 0);
					a.setText(String.valueOf(result));
				}
				break;
			default:
				Log.w(TAG, "Unknown request " + requestCode);
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated
	 * to the activity and potentially other fragments contained in that
	 * activity.
	 * <p>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface AboutListener {
		void showAbout();
	}
}
