package cz.gopas.kalkulacka;

import android.app.Activity;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ConstantActivity extends AppCompatActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_constant);
		RecyclerView recycler = findViewById(R.id.recycler);
		ConstantAdapter adapter = new ConstantAdapter();
		recycler.setAdapter(adapter);

		ConstantDatabase db = Room.databaseBuilder(
				getApplicationContext(),
				ConstantDatabase.class,
				"constants"
		).build();

		new Thread(() -> {
			for (int i = 0; i < 10_000; i++) {
				db.constantDao().insert(new ConstantModel("Item " + i, i));
			}
			List<ConstantModel> data = db.constantDao().getAll();
			runOnUiThread(() -> adapter.submitList(data));
		}).start();
	}

	class ConstantAdapter extends ListAdapter<ConstantModel, ConstantAdapter.ConstantViewHolder> {
		private final String TAG = ConstantAdapter.class.getSimpleName();

		@NonNull
		@Override
		public ConstantViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
			Log.d(TAG, "Create");
			return new ConstantViewHolder(
					LayoutInflater.from(parent.getContext())
							.inflate(android.R.layout.simple_list_item_2, parent, false)
			);
		}

		@Override
		public void onBindViewHolder(@NonNull ConstantViewHolder holder, int position) {
			Log.d(TAG, "Bind");
			ConstantModel model = getItem(position);
			holder.text1.setText(model.getName());
			holder.text2.setText(String.valueOf(model.getValue()));
			holder.itemView.setOnClickListener(v -> {
				Intent result = new Intent()
						.putExtra("result", model.getValue());
				setResult(Activity.RESULT_OK, result);
				finish();
			});
		}

		ConstantAdapter() {
			super(new DiffUtil.ItemCallback<ConstantModel>() {
				@Override
				public boolean areItemsTheSame(
						@NonNull ConstantModel oldContant, @NonNull ConstantModel newConstant) {
					// User properties may have changed if reloaded from the DB, but ID is fixed
					return oldContant.getValue() == newConstant.getValue();
				}

				@Override
				public boolean areContentsTheSame(
						@NonNull ConstantModel oldConstant, @NonNull ConstantModel newConstant) {
					// NOTE: if you use equals, your object must properly override Object#equals()
					// Incorrectly returning false here will result in too many animations.
					return oldConstant.equals(newConstant);
				}
			});
		}

		class ConstantViewHolder extends RecyclerView.ViewHolder {
			public TextView text1, text2;

			public ConstantViewHolder(View itemView) {
				super(itemView);
				text1 = itemView.findViewById(android.R.id.text1);
				text2 = itemView.findViewById(android.R.id.text2);
			}
		}
	}
}
