package cz.gopas.kalkulacka;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
data class ConstantModel(
	@PrimaryKey
	val name: String,
	val value: Float
)
